import './settings.css'

const Settings = ({open, onClose, children}) => {

  if(open) {
    return (
      <div className='SettingsContainer'>
        <div className='settings'>
          <h2>Speech settings</h2>
          {children}
          <span className='settings__close' onClick={onClose}>Done</span>
        </div>
      </div>
    )
  }
  return null
}

export default Settings